from pages.services_page import ServicesPage


# проверка отправки формы "с чего начать" (заказа проекта)
def test_should_send_form_ask_the_cost(browser):
    link = 'http://stage.idpowers.com/'
    page = ServicesPage(browser, link)
    page.open()
    page.go_to_services_page()
    page.find_element_btn_open_ask_form()
    page.click_on_btn_open_ask_form()
    page.find_field_name()
    page.give_keys_to_field_name()
    page.find_field_email()
    page.give_keys_to_field_email()
    page.find_field_message()
    page.give_keys_to_field_message()
    page.find_element_btn_send_form()
    page.click_btn_send_form()



