from selenium.webdriver.common.by import By


class ServicePageLocators:
    # кнопка "обсудить проект" на вкладке "услуги"
    BTN_SHOW_SEND_FORM_LINK = (By.CSS_SELECTOR, '.hero__btn.btn.js-show-form')
    # поле "имя" в форме "с чего начать", открывающей после нажатия на "обсудить проект"
    FIELD_NAME_LINK = (By.CSS_SELECTOR, '#popup-form-name')
    # поле "Email" в форме "с чего начать", открывающей после нажатия на "обсудить проект"
    FIELD_EMAIL_LINK = (By.CSS_SELECTOR, '#popup-form-email')
    # поле "сообщение" в форме "с чего начать", открывающей после нажатия на "обсудить проект"
    FIELD_MESSAGE_LINK = (By.CSS_SELECTOR, '#popup-form-message')
    # кнопка "отправить" в форме, открывающей после нажатия на "обсудить проект"
    BTN_SEND_LINK = (By.CSS_SELECTOR, '.btn.btn--block.wpcf7-submit')