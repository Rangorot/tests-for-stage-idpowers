from pages.base_page import BasePage
from locators.services_page_locators import ServicePageLocators
import time


class ServicesPage(BasePage):
    # поиск кнопки "обсудить проект"
    def find_element_btn_open_ask_form(self):
        btn = self.browser.find_element(*ServicePageLocators.BTN_SHOW_SEND_FORM_LINK)
        assert btn, 'No btn open ask form on page'
        return btn

    # нажатие на кнопку "обсудить проект"
    def click_on_btn_open_ask_form(self):
        self.find_element_btn_open_ask_form().click()

    # поиск поля "имя" в форме "с чего начать"
    def find_field_name(self):
        field = self.browser.find_element(*ServicePageLocators.FIELD_NAME_LINK)
        assert field, 'No field "name" in ask form'
        return field

    # заполнение поля "имя" в форме "с чего начать"
    def give_keys_to_field_name(self):
        self.find_field_name().send_keys(str(time.time()))

    # поиск поля "email" в форме "с чего начать"
    def find_field_email(self):
        field = self.browser.find_element(*ServicePageLocators.FIELD_EMAIL_LINK)
        assert field, 'No field "email" in ask form'
        return field

    # заполнения поля "email" в форме "с чего начать"
    def give_keys_to_field_email(self):
        self.find_field_email().send_keys(f'{str(time.time())}@faked.com')

    # поиск поля "сообщение" в форме "с чего начать"
    def find_field_message(self):
        field = self.browser.find_element(*ServicePageLocators.FIELD_MESSAGE_LINK)
        assert field, 'No field "message" in ask form'
        return field

    # заполнение поля "сообщение" в форме "с чего начать"
    def give_keys_to_field_message(self):
        self.find_field_message().send_keys(str(time.time()))

    #  поиск кнопки "отправить" в форме "с чего начать"
    def find_element_btn_send_form(self):
        btn = self.browser.find_element(*ServicePageLocators.BTN_SEND_LINK)
        assert btn, 'No btn send form on page'
        return btn

    # нажатие кнопки "отправить" в форме "с чего начать"
    def click_btn_send_form(self):
        self.find_element_btn_send_form().click()
