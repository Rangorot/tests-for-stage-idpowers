from selenium.webdriver.support.ui import WebDriverWait
from locators.base_page_locators import BasePageLocators


class BasePage:
    def __init__(self, browser, url, timeout=10):
        self.browser = browser
        self.url = url
        self.browser.implicitly_wait(timeout)

    # открывает страницу по переданному link
    def open(self):
        self.browser.get(self.url)

    # переход на страницу "услуг" по кнопке в главном меню
    def go_to_services_page(self):
        link = self.browser.find_element(*BasePageLocators.BTN_SERVICES_LINK)
        link.click()
